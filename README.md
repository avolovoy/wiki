# wiki

### Create a Wikipedia search app. 
On the first screen, show a UISearchBar in which the user can enter any search term. The app then queries the Wikipedia Search API and displays the results in a UITableView (on the same screen as the search bar). 

Each cell in the table view should display the page title from the API response as well as the last time it was edited (in a user-friendly date format in the device’s time zone).

When a user taps any row in the search results, open the Wikipedia page for that result in a WKWebView.

The Wikipedia API is complex and might be different than other APIs you’ve used before. Part of this challenge involves working through the documentation to figure out how to get the response you need.
Take as long as you need to present your work in the best light. In this exercise we’re more interested in code quality vs speed.

### Requirements

* Must be universal (runs on iPhone and iPad)
* Adaptive layout (i.e. can handle rotations)
* iOS 8 only​ (or iOS 9 beta only if you wish to use latest Swift)​
* Uses Core Data for offline caching of results so that the next time the user does the same search, the response is fast (and/or that search will work offline)
* Code should be well factored, readable, and—at least loosely—adhere to an accepted style guide
* Unit tested using a framework of your choice (XCTest preferred but not required)
* No third party libraries (except for possibly a testing framework)
* Written in Swift or Objective-C
* Publish the source code on GitHub​ (or other online git repo)​
* By default the API returns 10 results. Provide a way to retrieve subsequent pages of the search results beyond this first page of 10.

### Nice to have

* Written in Swift
* Custom UI of your choosing
