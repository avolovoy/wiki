//
//  WikiCoreDataStack.swift
//  wiki
//
//  Created by Alex Volovoy on 9/1/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

import Foundation
import CoreData
import UIKit

let dataModelName = "wiki"

public class WikiCoreDataStack {
    
    
    public class var sharedManager: WikiCoreDataStack {
        struct Singleton {
            static let coreDataStackManager = WikiCoreDataStack()
        }
        return Singleton.coreDataStackManager
    }
    
    private init() {
        // Initialize the stack
        let  _ = self.mainContext
    }
    
    // MARK: - Core Data stack
    lazy var storeURL: NSURL? = {
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent(dataModelName + ".sqlite")
        return url
        }()
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.volovoy.wiki" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("wiki", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        var error: NSError? = nil
        let options = [NSInferMappingModelAutomaticallyOption: true, NSMigratePersistentStoresAutomaticallyOption: true]
        
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: self.storeURL, options: options)
        } catch var error2 as NSError {
            error = error2
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            print("Unresolved error \(error), \(error!.userInfo)")
            print("Removing Core Data file !");
            do {
                // For this application storage data is not important - if we failed in migration - just recrate the file.
                try coordinator!.destroyPersistentStoreAtURL(self.storeURL!, withType: NSSQLiteStoreType, options: nil)
                print("Core Data file has been deleted");
                try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: self.storeURL, options: options)
                print("Core Data recreated");
            } catch var error1 as NSError {
                error = error1
            } catch {
                fatalError()
            }
            
        } catch {
            fatalError()
        }
        
        return coordinator
        }()
    
    // Context for the main Thread.
    lazy var mainContext: NSManagedObjectContext? = {
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext(concurrencyType:.MainQueueConcurrencyType)
        managedObjectContext.mergePolicy =  NSOverwriteMergePolicy
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    // Worker context
    public func newWorkerContext() -> NSManagedObjectContext {
        let workerContext = NSManagedObjectContext(concurrencyType:.PrivateQueueConcurrencyType)
        workerContext.parentContext = self.mainContext
        workerContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return workerContext;
    }
    
    // MARK: - Core Data Saving support
    public func saveWorkerContext (context: NSManagedObjectContext) {
        var error: NSError? = nil
        context.performBlockAndWait { () -> Void in
            if context.hasChanges {
                do {
                    try context.save()
                } catch let error1 as NSError {
                    error = error1
                    print("Unresolved error \(error), \(error!.userInfo)")
                } catch {
                    fatalError()
                }
            }
            if let _ = self.mainContext {
                self.saveContext()
            }
        }
        
    }
    
    public func saveContext() {
        if let moc = self.mainContext {
            var error: NSError? = nil
            moc.performBlock({ () -> Void in
                if moc.hasChanges {
                    do {
                        try moc.save()
                    } catch let error1 as NSError {
                        error = error1
                        print("Unresolved error \(error), \(error!.userInfo)")
                    } catch {
                        fatalError()
                    }
                }
            })
            
        }
    }
    
}