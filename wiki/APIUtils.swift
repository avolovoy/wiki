//
//  Networking.swift
//  wiki
//
//  Created by Alex Volovoy on 9/1/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

import Foundation
import CoreData


let wikiApiBaseUrl = "https://en.wikipedia.org/w/api.php"
let wikiSearchEndpoint = "?action=query&format=json&list=search&srwhat=text&srprop=timestamp&srsearch="
let wikiOpenTitleBaseUrl = "https://en.wikipedia.org/w/index.php?title="

enum WikiError : String, ErrorType {
    case NODATA = "No data in response"
    case REQUESTFAILED = "Request failed"
    case JSONERROR = "Not valid JSON response"
    case COREDATAERROR = "Core Data Error"

}

func parseJSONDictionary ( json json: Dictionary<String, AnyObject>, queryString : String ) throws {
    let workerContext = WikiCoreDataStack.sharedManager.newWorkerContext()
    print("\(json)")

    guard let query = json["query"] as? Dictionary<String, AnyObject>,
        let searchHits = query["search"] as? [AnyObject] else {
            throw WikiError.JSONERROR
    }
    guard let searchQuery = SearchQuery.searchQuery(queryString, context: workerContext) else {
        throw WikiError.COREDATAERROR
    }

    if let continueSearch = json["continue"] as? Dictionary<String, AnyObject>,
        let scrollOffset = continueSearch["sroffset"] as? NSNumber,
        let continueStr = continueSearch["continue"] as? String {
            searchQuery.continueSearch = continueStr.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())
            searchQuery.sroffset = scrollOffset
    }
    
    for searchHit in searchHits {
        guard let wikiPage = searchHit as?  Dictionary<String, AnyObject>,
            let title = wikiPage["title"] as? String ,
            let timestamp = wikiPage["timestamp"] as? String else {
                throw WikiError.JSONERROR
        }
        if let wikiPageObject = WikiPage.pageWithTitle(title, context: workerContext) {
            wikiPageObject.query = searchQuery
            if let date = dateFromString(timestamp){
                wikiPageObject.timeStamp = date
                print("\(title),\(timestamp)")
            }
        }
    }
    do {
        try workerContext.save()
    }
}

/*
* Search Wiki with the query string.  When a search successfully
* completes, optional completionBlock will be called on the main thread
*/

func searchWiki(query query: String, continueSearch:Bool, completionHandler: ((success: Bool) -> Void)?) {
    let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
    let session = NSURLSession(configuration: configuration)
    if let queryString = query.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet()) {
        var continueParams = "&continue="
        if continueSearch {
            if let searchQuery = SearchQuery.searchQuery(queryString, context: WikiCoreDataStack.sharedManager.mainContext!)  {
                if let continueVal = searchQuery.continueSearch {
                    continueParams += continueVal
                }
                if let scrollOffset = searchQuery.sroffset {
                    continueParams += "&sroffset=\(scrollOffset)"
                }
                
            }
   
        }
        let url = NSURL(string:wikiApiBaseUrl+wikiSearchEndpoint + queryString + continueParams)
        print("Searching \(url)")
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "GET"
        session.dataTaskWithRequest(request) {
            data, response, error in
            do {
                guard let httpResponse = response as? NSHTTPURLResponse where
                    200 ... 299 ~= httpResponse.statusCode else {
                    throw WikiError.REQUESTFAILED
                }
                guard let responseData = data  else {
                    throw WikiError.NODATA
                }
                guard let jsonResponse  = try NSJSONSerialization.JSONObjectWithData(responseData, options: []) as? [String:AnyObject] else {
                    throw WikiError.JSONERROR
                }
                try parseJSONDictionary(json: jsonResponse, queryString: query)
                if let completionBlock = completionHandler {
                    dispatch_async(dispatch_get_main_queue()){
                        completionBlock(success: true)
                    }
                }
            } catch  let error as WikiError {
                print(error.rawValue)
                if let completionBlock = completionHandler {
                    dispatch_async(dispatch_get_main_queue()){
                        completionBlock(success: false)
                    }
                }
            } catch {
                 print(error)
            }
            
            }.resume()
        
    }
    
}

