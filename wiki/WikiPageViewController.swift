//
//  WikiPageViewController.swift
//  wiki
//
//  Created by Alex Volovoy on 9/1/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

import UIKit
import WebKit

class WikiPageViewController: UIViewController, WKNavigationDelegate, ProgressViewDelegate {

    var webView : WKWebView!
    var progressView : ProgressView!

    @IBOutlet weak var containerView: UIView!
    
    var wikiPageTitle: String? {
        didSet {
            if let title = wikiPageTitle {
                self.title = title
             } else {
                self.title = ""

            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView = WKWebView(frame: self.view.bounds)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.navigationDelegate = self
        view.addSubview(webView)
        progressView = ProgressView.loadFromNibNamed("ProgressView") as! ProgressView
        progressView.frame = view.bounds
        progressView.delegate = self
        progressView.hidden = true
        view.addSubview(progressView)

        let views : [String:AnyObject] = ["webview":webView,"topLayoutGuide":self.topLayoutGuide, "bottomLayoutGuide":self.bottomLayoutGuide, "progressView":progressView]
        view.addConstraints(
            NSLayoutConstraint.constraintsWithVisualFormat("H:|[webview]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views:views))
        view.addConstraints(
            NSLayoutConstraint.constraintsWithVisualFormat("V:[topLayoutGuide][webview][bottomLayoutGuide]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views:views))
        view.addConstraints(
            NSLayoutConstraint.constraintsWithVisualFormat("H:|[progressView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views:views))
        view.addConstraints(
            NSLayoutConstraint.constraintsWithVisualFormat("V:|[progressView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views:views))
     
        
            if let urlStr = wikiPageTitle?.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet()){
            if let url =  NSURL(string: wikiOpenTitleBaseUrl + urlStr) {
                webView.loadRequest(NSURLRequest(URL:url))
            }

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func configureProgressView(showView showView: Bool, empty: Bool) {
        let message = showView ? "Loading..." : "Oops.. looks there is problem loading the page"
        progressView.configureProgressView(showView, parentView: view, message: message, isEmpty:empty)
    }
    
    // MARK WKNavigation Delegate
    
    func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        configureProgressView(showView:true, empty: false)
    }
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        configureProgressView(showView:false, empty: false)
    }
    
    func webView(webView: WKWebView, didFailNavigation navigation: WKNavigation!, withError error: NSError) {
        configureProgressView(showView:true, empty: true)
    }
    
    // Mark ProgressView Delegate
    func refreshData() {
        if let _ = wikiPageTitle {
            webView.reload()
        }
        
    }


}

