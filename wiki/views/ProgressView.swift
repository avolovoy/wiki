//
//  ProgressView.swift
//  wiki
//
//  Created by Alex Volovoy on 9/4/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

import UIKit

protocol ProgressViewDelegate : class {
    func refreshData()
}

class ProgressView: UIView {
 
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var emptyDataLabel: UILabel!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    @IBOutlet weak var updatingLabel: UILabel!
    
    weak var delegate : ProgressViewDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        let color = mainBlueColor
        progressIndicator.color = color
        updatingLabel.textColor = color
        actionButton.tintColor = color
        actionButton.layer.cornerRadius = 3.0
        actionButton.layer.borderWidth = 1.0
        actionButton.layer.borderColor = color.CGColor
        actionButton.layer.masksToBounds = true
    }
    
    func configureProgressView (updating:Bool, parentView: UIView, message:String?, isEmpty:Bool = false, noAction:Bool = false) {
        if updating {
            self.hidden = false
            progressIndicator.startAnimating()
            emptyDataLabel.hidden = true
            actionButton.hidden = true

            if let labelMessage = message {
                updatingLabel.hidden = false
                updatingLabel.text = labelMessage
            } else {
                updatingLabel.hidden = true
            }
            
        } else {
            progressIndicator.stopAnimating()
            updatingLabel.hidden = true
            if isEmpty {
                if let labelMessage = message {
                    emptyDataLabel.hidden = false
                    emptyDataLabel.text = labelMessage
                }
                
                if let _ = delegate {
                     actionButton.hidden = noAction
                } else {
                    actionButton.hidden = true
                }
            
            } else {
                self.hidden = true
            }
        }
        
    }
    
    
    @IBAction func actionButtonTapped(sender: AnyObject) {
        if let hasDelegate = delegate {
           hasDelegate.refreshData()
        }
    }

}
