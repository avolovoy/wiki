//
//  WikiTableViewCell.swift
//  wiki
//
//  Created by Alex Volovoy on 9/4/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import UIKit

class WikiTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var updatedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let bgColorView = UIView()
        bgColorView.backgroundColor = selectionBlueColor
        self.selectedBackgroundView = bgColorView
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
