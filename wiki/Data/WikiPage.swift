//
//  WikiPage.swift
//  wiki
//
//  Created by Alex Volovoy on 9/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import Foundation
import CoreData

class WikiPage: NSManagedObject {
    static let entityName = "WikiPage"
    static func pageWithTitle (title: String, context: NSManagedObjectContext) -> WikiPage? {
        var wikiPage : WikiPage?
        context.performBlockAndWait { () -> Void in
            let predicate = NSPredicate(format: "title == %@", title)
            if let existingPage = context.lookupObject(entityName, predicate: predicate) as? WikiPage{
                wikiPage = existingPage
            } else {
                if let newPage = NSEntityDescription.insertNewObjectForEntityForName(WikiPage.entityName, inManagedObjectContext: context) as? WikiPage {
                    newPage.title = title
                    wikiPage = newPage
                }
            }
  
        }
        return wikiPage
        
    }
    
}
