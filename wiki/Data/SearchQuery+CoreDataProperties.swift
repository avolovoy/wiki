//
//  SearchQuery+CoreDataProperties.swift
//  wiki
//
//  Created by Alex Volovoy on 9/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SearchQuery {

    @NSManaged var continueSearch: String?
    @NSManaged var queryString: String?
    @NSManaged var sroffset: NSNumber?
    @NSManaged var hits: NSOrderedSet?

}
