//
//  WikiPage+CoreDataProperties.swift
//  wiki
//
//  Created by Alex Volovoy on 9/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension WikiPage {

    @NSManaged var timeStamp: NSDate?
    @NSManaged var title: String?
    @NSManaged var query: SearchQuery?

}
