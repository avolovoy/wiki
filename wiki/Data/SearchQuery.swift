//
//  SearchQuery.swift
//  wiki
//
//  Created by Alex Volovoy on 9/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import Foundation
import CoreData

class SearchQuery: NSManagedObject {

    static let entityName = "SearchQuery"
    static func searchQuery (query: String, context: NSManagedObjectContext) -> SearchQuery? {
        var searchQuery : SearchQuery?
        context.performBlockAndWait { () -> Void in
            let predicate = NSPredicate(format: "queryString == %@", query)
            if let existingQuery = context.lookupObject(entityName, predicate: predicate) as? SearchQuery{
                searchQuery = existingQuery
            } else {
                if let newQuery = NSEntityDescription.insertNewObjectForEntityForName(SearchQuery.entityName, inManagedObjectContext: context) as? SearchQuery {
                    newQuery.queryString = query
                    searchQuery = newQuery
                }
            }
            
        }
        return searchQuery
        
    }
}
