//
//  Extentions.swift
//  wiki
//
//  Created by Alex Volovoy on 9/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import Foundation
import CoreData
import UIKit

extension UIView {
    class func loadFromNibNamed(nibNamed: String, bundle : NSBundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiateWithOwner(nil, options: nil).first as? UIView
    }
}

enum CoreDataError : String, ErrorType {
    case DBERROR = "CoreData Error"
}

extension NSManagedObjectContext {
    
    func lookupObject(entity: String, predicate: NSPredicate) -> NSManagedObject? {
        do {
            let fetchRequest = NSFetchRequest(entityName: entity)
            fetchRequest.predicate = predicate
            if let results = try self.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
                return results.first
            }
        } catch {
            return nil
        }
     
        return nil
    }
    
}