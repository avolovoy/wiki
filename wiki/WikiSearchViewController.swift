//
//  MasterViewController.swift
//  wiki
//
//  Created by Alex Volovoy on 9/1/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

import UIKit
import CoreData

class WikiSearchViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    var detailViewController: WikiPageViewController? = nil
    var searchQueryString : String = ""
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.clearsSelectionOnViewWillAppear = false
            self.preferredContentSize = CGSize(width: 320.0, height: 600.0)
        }
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        let cellNib = UINib(nibName: "WikiTableViewCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: "WikiCell")
        let moreNib = UINib(nibName: "MoreTableViewCell", bundle: nil)
        tableView.registerNib(moreNib, forCellReuseIdentifier: "MoreCell")
        
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? WikiPageViewController
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = true
        super.viewWillAppear(animated)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Segues
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                if let object = self.fetchedResultsController.objectAtIndexPath(indexPath) as? WikiPage {
                    let controller = (segue.destinationViewController as! UINavigationController).topViewController as! WikiPageViewController
                    controller.wikiPageTitle = object.title
                    controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                    controller.navigationItem.leftItemsSupplementBackButton = true
                    // Hide Master on the selection
                    if view.traitCollection.userInterfaceIdiom == .Pad && splitViewController?.displayMode == .PrimaryOverlay {
                        let animations: () -> Void = {
                            self.splitViewController?.preferredDisplayMode = .PrimaryHidden
                        }
                        let completion: Bool -> Void = { _ in
                            self.splitViewController?.preferredDisplayMode = .Automatic
                        }
                        UIView.animateWithDuration(0.3, animations: animations, completion: completion)
                    }
                }
                
            }
        }
    }
    
    // MARK: - Table View
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        if sectionInfo.numberOfObjects > 0 {
            return sectionInfo.numberOfObjects + 1
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let sectionInfo = self.fetchedResultsController.sections![indexPath.section]
        if indexPath.item == sectionInfo.numberOfObjects {
            return tableView.dequeueReusableCellWithIdentifier("MoreCell", forIndexPath: indexPath)
        }
        let cell = tableView.dequeueReusableCellWithIdentifier("WikiCell", forIndexPath: indexPath) as! WikiTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(cell: WikiTableViewCell, atIndexPath indexPath: NSIndexPath) {
        if let wikiPage = self.fetchedResultsController.objectAtIndexPath(indexPath) as? WikiPage {
            cell.titleLabel!.text = wikiPage.title
            if let date = wikiPage.timeStamp {
                cell.updatedLabel!.text = "Updated: \(stringFromDate(date))"
            } else {
                cell.updatedLabel!.text  = ""
            }
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let sectionInfo = self.fetchedResultsController.sections![indexPath.section]
        if indexPath.item == sectionInfo.numberOfObjects  {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            searchWikipedia(query:searchQueryString, contunueSearch:true)
        } else {
            let cell = tableView.cellForRowAtIndexPath(indexPath)! as! WikiTableViewCell
            self.performSegueWithIdentifier("showDetail", sender: cell)
        }
    }
    
    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        let entity = NSEntityDescription.entityForName(WikiPage.entityName, inManagedObjectContext: WikiCoreDataStack.sharedManager.mainContext!)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        // Predicate
        let predicate = NSPredicate(format: "query.queryString == %@", self.searchQueryString)
        fetchRequest.predicate = predicate
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "timeStamp", ascending: false)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: WikiCoreDataStack.sharedManager.mainContext!, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch  {
            print("Unresolved error \(error)")
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController? = nil
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Delete:
            self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        default:
            return
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        case .Update:
            self.configureCell(tableView.cellForRowAtIndexPath(indexPath!)! as! WikiTableViewCell, atIndexPath: indexPath!)
        case .Move:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        }
    }
    
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
    
    func resetFetch() {
        do {
            let predicate = NSPredicate(format: "query.queryString == %@", self.searchQueryString)
            self.fetchedResultsController.fetchRequest.predicate = predicate
            try self.fetchedResultsController.performFetch()
            self.tableView.reloadData()
        } catch {
            print("Unresolved error \(error)")
        }
    }
    
    // MARK UISearchBar Delegate
    // return NO to not become first responder
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        return true
    }
    // called when text starts editing
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    // return NO to not resign first responder
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        return true
    }
    // called when text ends editing
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        
    }
    // called when text changes (including clear)
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    // called when keyboard search button
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if let text = searchBar.text {
            searchQueryString = text
            searchWikipedia(query:searchQueryString, contunueSearch:false)
        }
        searchBar.resignFirstResponder()
    }
    // called when cancel button pressed
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchQueryString = ""
        searchBar.resignFirstResponder()
        self.resetFetch()
    }
    
    // MARK Networking
    func searchWikipedia (query query: String, contunueSearch: Bool) {
        searchWiki(query:query, continueSearch: contunueSearch) { (success) -> Void in
            self.resetFetch()
        }
    }
    
}

