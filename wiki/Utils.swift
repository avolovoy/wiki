//
//  Utils.swift
//  wiki
//
//  Created by Alex Volovoy on 9/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import Foundation
import UIKit

let mainBlueColor = UIColor(red: 51/256, green: 152/256, blue: 219/256, alpha: 1)
let selectionBlueColor = UIColor(red: 51/256, green: 152/256, blue: 219/256, alpha: 0.5)


public func stringFromDate(date: NSDate) -> String {
    let dateFormatter: NSDateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss"
    dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
    
    return dateFormatter.stringFromDate(date)
    
    
}

public func dateFromString(dateString: NSString) -> NSDate? {
    let dateFormatter: NSDateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
    
    return dateFormatter.dateFromString(dateString as String)
}