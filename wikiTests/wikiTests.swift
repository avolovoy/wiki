//
//  wikiTests.swift
//  wikiTests
//
//  Created by Alex Volovoy on 9/1/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

import UIKit
import XCTest
@testable import wiki

class wikiTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAPILink () {
        let expectation = self.expectationWithDescription("API Test")
        searchWiki(query:"Deep purple", continueSearch: false) { (success) -> Void in
            expectation.fulfill()
            XCTAssertTrue(success, "Query and/or import data failed")
        }
        self.waitForExpectationsWithTimeout(30, handler: nil);
    }
    
}
